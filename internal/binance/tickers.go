package binance

import (
	"ccx/pkg"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers() []pkg.Ticker {

	u, err := url.Parse(BaseUrl + "/api/v3/ticker/bookTicker")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    map[string]interface{}{},
	}
	
	data := r.Call()

	parsed := data.([]interface{})
	
	symbolsList := GetSymbols()
	
	var tickers []pkg.Ticker
	var coin string
	var cur string

	volumeData := Get24HData()

	for _, i := range parsed {

		reparsed := i.(map[string]interface{})

		symbol := reparsed["symbol"].(string)

		for _, s := range symbolsList{
			
			i := strings.Join(strings.Split(s, "/"), "")		
			
			if strings.EqualFold(i, symbol) {				
				coin = strings.Split(s, "/")[0]
				cur = strings.Split(s, "/")[1]
				bidPrice, err := strconv.ParseFloat(reparsed["bidPrice"].(string), 64)
				pkg.ErrorCheck(err)
				bidQty, err := strconv.ParseFloat(reparsed["bidQty"].(string), 64)
				pkg.ErrorCheck(err)
				askPrice, err := strconv.ParseFloat(reparsed["askPrice"].(string), 64)
				pkg.ErrorCheck(err)
				askQty, err := strconv.ParseFloat(reparsed["askQty"].(string), 64)
				pkg.ErrorCheck(err)
				
				tickers = append(tickers, pkg.Ticker{
					Symbol: pkg.Symbol{Coin: coin, Currency: cur},
					BidPrice: bidPrice,
					BidQty: bidQty,
					AskPrice: askPrice,
					AskQty: askQty,
					BaseVolume: volumeData[symbol][0],
					QuoteVolume: volumeData[symbol][1],
					Exchange: "binance",
					Timestamp: int(time.Now().Unix())})
			}
		}
	}
	if len(tickers) == 0 {
		log.Println("Could not get tickers from binance.")
	}
	return tickers
}