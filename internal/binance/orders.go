package binance

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strings"
	"time"
)

// CreateOrder ...
func CreateOrder(key, secret, side, symbol string, price, amount float64) string {

	endoint := "/api/v3/order"

	timestamp := time.Now().Unix() * 1000

	parsedSymbol := strings.Join(strings.Split(symbol, "/"), "")

	body := fmt.Sprintf("symbol=%s&side=%s&timeInForce=GTC&type=LIMIT&price=%f&quantity=%f&recvWindow=5000&timestamp=%d", 
		parsedSymbol, strings.ToUpper(side), price, amount, timestamp,
	)
	
	signature:= pkg.SignWithSecret(body, secret, "sha256")
	
	fullUrl := fmt.Sprintf("%s%s?%s&signature=%s", BaseUrl, endoint, body, signature)

	u, err := url.Parse(fullUrl)

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{"X-MBX-APIKEY": key},
		Body:    nil,
	}
	
	req := r.Call()

	d := req.(map[string]interface{})

	if d["orderId"] == nil {
		return ""
	}

	return d["orderId"].(string)
}

// CancelOrder ...
func CancelOrder (key, secret, symbol, orderId string) bool {
	
	endoint := "/api/v3/order"

	timestamp := time.Now().Unix() * 1000

	parsedSymbol := strings.Join(strings.Split(symbol, "/"), "")

	body := fmt.Sprintf("orderId=%ssymbol=%s&recvWindow=5000&timestamp=%d", orderId, parsedSymbol, timestamp)
	
	signature:= pkg.SignWithSecret(body, secret, "sha256")
	
	fullUrl := fmt.Sprintf("%s%s?%s&signature=%s",BaseUrl, endoint, body, signature)

	u, err := url.Parse(fullUrl)

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{"X-MBX-APIKEY": key},
		Body:    nil,
	}
	
	req := r.Call()

	d := req.(map[string]interface{})

	return  d["status"] == "CANCELED"
}

// CheckOrder ...
func CheckOrder (key, secret, symbol, orderId string) interface{} {
	
	endoint := BaseUrl + "/api/v3/order"

	timestamp := time.Now().Unix() * 1000

	parsedSymbol := strings.Join(strings.Split(symbol, "/"), "")

	body := fmt.Sprintf("orderId=%ssymbol=%s&recvWindow=5000&timestamp=%d", orderId, parsedSymbol, timestamp)
	
	signature:= pkg.SignWithSecret(body, secret, "sha256")
	
	fullUrl := fmt.Sprintf("%s%s?%s&signature=%s", BaseUrl, endoint, body, signature)

	u, err := url.Parse(fullUrl)

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{"X-MBX-APIKEY": key},
		Body:    nil,
	}
	
	req := r.Call()	

	return req
}