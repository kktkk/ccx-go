package binance

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strconv"
	"time"
)

// GetBalances ...
func GetBalances (key, secret string) (map[string]float64) {

	balances := make(map[string]float64)

	queryString := "/api/v3/account"

	timestamp := time.Now().Unix() * 1000

	body := fmt.Sprintf("recvWindow=5000&timestamp=%d", timestamp)

	signature:= pkg.SignWithSecret(body, secret, "sha256")

	fullUrl := fmt.Sprintf("%s%s?%s&signature=%s", BaseUrl, queryString, body, signature)

	u, err := url.Parse(fullUrl)

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"X-MBX-APIKEY": key},
		Body:    nil,
	}

	req := r.Call()

	bals := req.(map[string]interface{})["balances"]

	if bals != nil {
		for _, i := range bals.([]interface{}) {
			d := i.(map[string]interface{})
			bal, err := strconv.ParseFloat(d["free"].(string), 64)
			pkg.ErrorCheck(err)
			if bal > 0.0 {
				balances[d["asset"].(string)] = bal
			}
		}
	}

	return balances
}