package binance_test

import (
	"ccx/internal/binance"
	"log"
	"testing"
)

var key = ""
var secret = ""

func TestGetSymbols (t *testing.T) {
	s := binance.GetSymbols()
	// log.Println(s)
	if len(s) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(s))
	}
}

func TestGetTickers (t *testing.T) {
	tickers := binance.GetTickers()
	// log.Println(tickers)
	if len(tickers) == 0 {
		t.Errorf("Expected lenght of tickers bigger than 0, got %v", len(tickers) )
	}
}

func TestGetBalances (t *testing.T) {
	balances := binance.GetBalances(key, secret)
	log.Println(balances)
	if len(balances) == 0 {
		t.Errorf("Expected lenght of balances bigger than 0, got %v", len(balances) )
	}
}

func TestCreateOrder (t *testing.T) {
	orderId := binance.CreateOrder(key, secret, "buy", "BTC/USDT", 35000, 0.1)
	log.Println(orderId)
	if orderId == "" {
		t.Errorf("Expected orderId, got %v", orderId )
	}
}