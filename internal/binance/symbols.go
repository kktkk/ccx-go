package binance

import (
	"ccx/pkg"
	"net/url"
	"strings"
)

// GetSymbols ...
func GetSymbols() (symbols []string) {
	
	u, err := url.Parse(BaseUrl + "/api/v3/exchangeInfo")

	pkg.ErrorCheck(err)
	
	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    map[string]interface{}{},
	}

	data := r.Call()
	
	parsed := data.(map[string]interface{})
	
	symbolsList := parsed["symbols"].([]interface{})
	
	for _, s := range symbolsList {
		
		if strings.EqualFold(s.(map[string]interface{})["status"].(string), "trading") {
			
			for _, t := range s.(map[string]interface{})["permissions"].([]interface{}) {
				
				if strings.EqualFold(t.(string), "spot") {

					sym := pkg.Symbol{
						Coin:     s.(map[string]interface{})["baseAsset"].(string),
						Currency: s.(map[string]interface{})["quoteAsset"].(string),
					}
					
					symbol, err := sym.SymbolConverter()

					pkg.ErrorCheck(err)

					symbols = append(symbols, symbol.(string))
				}
			}
		}
	}
	
	return symbols
}