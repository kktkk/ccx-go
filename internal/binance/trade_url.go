package binance

var marketsURLBase = "https://www.binance.com/en/trade/"

func GenerateTradeUrl (coin, cur string) string {
	url := marketsURLBase + coin + "_" + cur
	return url
}