package bitfinex

import (
	"ccx/pkg"
	"encoding/json"
	"fmt"
	"net/url"
	"strings"
	"time"
)

// CreateOrder...
func CreateOrder (key, secret, side, symbol string, price, amount float64) (string) {
	
	endpoint := "v2/auth/w/order/submit"
	
	u, err := url.Parse(AuthBaseUrl + endpoint)

	pkg.ErrorCheck(err)
	
	timestamp := time.Now().Unix() * 1000
	
	qty := amount
	if strings.EqualFold(side, "sell") {
		qty = amount * -1
	}

	symbolList := GetSymbolsData()

	pair := BitfinexSymbolConverter(symbol, symbolList)

	body := map[string]interface{}{
		"type": "LIMIT",
		"symbol": pair,
		"price": fmt.Sprintf("%f", price),
		"amount": fmt.Sprintf("%f", qty),
	}

	bodyJson, err := json.Marshal(body)

	pkg.ErrorCheck(err)

	signature := fmt.Sprintf("/api/%s%d%s", endpoint, timestamp, string(bodyJson))

	sig := pkg.SignWithSecret(signature, secret, "sha384")

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"bfx-nonce": fmt.Sprintf("%d", timestamp),
			"bfx-apikey": key,
			"bfx-signature": sig,
		},
		Body:    body,
	}

	data := r.Call()

	if data.([]interface{})[0] == "error" {
		return data.([]interface{})[len(data.([]interface{})) - 1].(string)
	}

	return data.([][][]interface{})[4][0][0].(string)
}

// CheckOrder ...
func CheckOrder (key, secret, symbol, orderId string) interface{} {
	
	timestamp := time.Now().Unix() * 1000

	endpoint := "v2/auth/r/orders"
	
	u, err := url.Parse(AuthBaseUrl + endpoint)
	
	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"bfx-nonce": fmt.Sprintf("%d", timestamp),
			"bfx-apikey": key,
			"bfx-signature": "",
		},
		Body:    nil,
	}

	if len(orderId) > 0 {
		
		body := map[string]interface{}{
			"id": fmt.Sprintf("[%s]", orderId),
		}

		bodyJson, err := json.Marshal(body)

		pkg.ErrorCheck(err)
	
		signature := fmt.Sprintf("/api/%s%d%s", endpoint, timestamp, bodyJson)
	
		sig := pkg.SignWithSecret(signature, secret, "sha384")

		r.Headers["bfx-signature"] = sig

		r.Body = body

	} else {	
		
		signature := fmt.Sprintf("/api/%s%d", endpoint, timestamp)
	
		sig := pkg.SignWithSecret(signature, secret, "sha384")

		r.Headers["bfx-signature"] = sig
	}

	return r.Call()
}

// CancelOrder ... 
func CancelOrder (key, secret, symbol, orderId string) bool {
	
	timestamp := time.Now().Unix() * 1000

	endpoint := "v2/auth/w/order/cancel"
	
	u, err := url.Parse(AuthBaseUrl + endpoint)

	pkg.ErrorCheck(err)

	body := map[string]interface{}{
		"id": fmt.Sprintf("[%s]", orderId),
	}

	bodyJson, err := json.Marshal(body)

	pkg.ErrorCheck(err)

	signature := fmt.Sprintf("/api/%s%d%s", endpoint, timestamp, bodyJson)

	sig := pkg.SignWithSecret(signature, secret, "sha384")

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"bfx-nonce": fmt.Sprintf("%d", timestamp),
			"bfx-apikey": key,
			"bfx-signature": sig,
		},
		Body:    body,
	}

	data := r.Call()

	if data.([]interface{})[0] == "error" {
		return false
	}

	return data.([]interface{})[4].([]interface{})[13].(string) == "CANCELED"
}