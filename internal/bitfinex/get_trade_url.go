package bitfinex

// GetTradeUrl ...
func GetTradeUrl (coin, cur string) string {
	url := "https://trading.bitfinex.com/t/" + coin + ":" + cur
	return url
}