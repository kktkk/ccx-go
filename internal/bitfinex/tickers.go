package bitfinex

import (
	"ccx/pkg"
	"net/url"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers() (tickers []pkg.Ticker) {

	u, err := url.Parse(BaseUrl + "v2/tickers?symbols=ALL")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"Content-Type": "application/json"},
		Body:    nil,
	}
	
	data := r.Call()
	
	parsed := data.([]interface{})

	symbols := GetSymbolsData()

	symbolData := symbols.([]interface{})[1]

	for _, i := range parsed {
		
		reparsed := i.([]interface{})
		
		rawSymbol := reparsed[0].(string)
		
		var coin string
		var cur string
		
		if strings.Index(rawSymbol, "t") == 0 {
			
			s := rawSymbol[1:]

			if strings.Contains(s, ":") {
				coin = SymbolConverter(strings.Split(s, ":")[0], symbolData)
				cur = SymbolConverter(strings.Split(s, ":")[1], symbolData)
			} else {
				coin = SymbolConverter(s[:3], symbolData)
				cur = SymbolConverter(s[3:], symbolData)
			}

			bidPrice := reparsed[1].(float64)
			bidQty := reparsed[2].(float64)
			askPrice := reparsed[3].(float64)
			askQty := reparsed[4].(float64)
			baseVol := reparsed[8].(float64)
			quoteVol := 0.0
		
			tickers = append(tickers, pkg.Ticker{
				Symbol: pkg.Symbol{Coin:coin, Currency: cur},
				BidPrice: bidPrice,
				BidQty: bidQty,
				AskPrice: askPrice,
				AskQty: askQty,
				BaseVolume: baseVol,
				QuoteVolume: quoteVol,
				Exchange: "bitfinex",
				Timestamp: int(time.Now().Unix())})
		}
	}
	return tickers
}
