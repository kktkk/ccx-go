package bitfinex

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"time"
)

//GetBalances ...
func GetBalances (key, secret string) (map[string]float64) {
	
	balances := make(map[string]float64)

	timestamp := time.Now().Unix() * 1000

	endpoint := "v2/auth/r/wallets"
	
	u, err := url.Parse(AuthBaseUrl + endpoint)

	pkg.ErrorCheck(err)

	signature := fmt.Sprintf("/api/%s%d", endpoint, timestamp)

	sig := pkg.SignWithSecret(signature, secret, "sha384")

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"bfx-nonce": fmt.Sprintf("%d", timestamp),
			"bfx-apikey": key,
			"bfx-signature": sig,
		},
		Body:    nil,
	}

	data := r.Call()

	for _, v := range data.([]interface{}) {
		if v.([]interface{})[0].(string) == "exchange" {
			cur := v.([]interface{})[1].(string)
			bal := v.([]interface{})[4].(float64)
			balances[cur] = bal
		}
	}
	return balances
}