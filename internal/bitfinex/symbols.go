package bitfinex

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strings"
)

// SymbolConverter ...
func SymbolConverter (symbol string, symbolList interface{}) (string) {

	for _, set := range symbolList.([]interface{}) {
		if strings.EqualFold(set.([]interface{})[0].(string), symbol) {
			return set.([]interface{})[1].(string)
		} else {
			return symbol
		}
	}
	return ""
}

// BitfinexSymbolConverter ...
func BitfinexSymbolConverter (symbol string, symbolList interface{}) (string) {

	coin := strings.Split(symbol, "/")[0]
	cur := strings.Split(symbol, "/")[1]
	for _, set := range symbolList.([]interface{}) {
		if strings.EqualFold(set.([]interface{})[1].(string), coin) {
			coin = set.([]interface{})[0].(string)
		}
		if strings.EqualFold(set.([]interface{})[1].(string), cur) {
			cur = set.([]interface{})[0].(string)
		}
	}
	return fmt.Sprintf("t%s%s", coin, cur)
}

// GetSymbolsData ...
func GetSymbolsData () interface{} {

	u, err := url.Parse(BaseUrl + "v2/conf/pub:list:pair:exchange,pub:map:currency:sym")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"Content-Type": "application/json"},
		Body:    nil,
	}

	data := r.Call()

	return data
}

// GetSymbols...
func GetSymbols () (symbols []string) {
	
	data := GetSymbolsData()

	for _, pair := range data.([]interface{})[0].([]interface{}) {
		
		symbolList := data.([]interface{})[1].([]interface{})
		
		s := pkg.Symbol{
			Coin:     "coin",
			Currency: "currency",
		}

		if strings.Contains(pair.(string), ":") {
			s.Coin = SymbolConverter(strings.Split(pair.(string), ":")[0], symbolList)
			s.Currency = SymbolConverter(strings.Split(pair.(string), ":")[1], symbolList)
		} else {
			s.Coin = SymbolConverter(pair.(string)[:3], symbolList)
			s.Currency = SymbolConverter(pair.(string)[3:], symbolList)
		}

		symbolString, err := s.SymbolConverter()
		
		pkg.ErrorCheck(err)

		symbols = append(symbols, symbolString.(string))
	}

	return symbols
}