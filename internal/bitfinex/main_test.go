package bitfinex_test

import (
	"ccx/internal/bitfinex"
	"log"
	"testing"
)

var key = ""
var secret = ""

func TestGetSymbols (t *testing.T) {
	s := bitfinex.GetSymbols()
	if len(s) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %d", len(s))
	}
}

func TestGetTickers (t *testing.T) {
	tickers := bitfinex.GetTickers()
	log.Println(tickers)
	if len(tickers) == 0 {
		t.Errorf("Expected lenght of tickers bigger than 0, got %d", len(tickers) )
	}
}

func TestGetBalances (t *testing.T) {
	bal:= bitfinex.GetBalances(key, secret)
	log.Println(bal)
	if len(bal) == 0 {
		t.Errorf("Expected lenght of balances bigger than 0, got %d", len(bal) )
	}
}

var currentOrder string

func TestCreateOrder (t *testing.T) {
	orderId:= bitfinex.CreateOrder(key, secret, "buy", "BTC/USD", 35000, 0.001)
	log.Println(orderId)
	currentOrder = orderId
	if len(orderId) == 0 {
		t.Errorf("Expected lenght of orderId bigger than 0, got %d", len(orderId) )
	}
}

func TestCheckOrder (t *testing.T) {
	order:= bitfinex.CheckOrder(key, secret, "BTC/USD", "")
	log.Println(order)
	if len(order.([]interface{})) == 0 {
		t.Errorf("Expected order lenght to be greater than 0, got %d", len(order.([]interface{})) )
	}
}

func TestCancelOrder (t *testing.T) {
	success:= bitfinex.CancelOrder(key, secret, "BTC/USD", currentOrder)
	log.Println(success)
	if !success {
		t.Errorf("Expected cancel order result to be true, got %v", success )
	}
}