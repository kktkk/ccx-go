package graviex

import (
	"ccx/pkg"
	"net/url"
	"strings"
)

// WalletStatus ...
var WalletStatus = make(map[string]bool)

// GetWalletStatus ...
func GetWalletStatus (cur string) (bool) {

	for n := range WalletStatus {
		if n == cur {
			return WalletStatus[cur]
		}
	}

	u, err := url.Parse(BaseUrl + "/webapi/v3/currency/info.json?currency=" + strings.ToLower(cur))

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"Content-Type": "application/json"},
		Body:    nil,
	}

	req := r.Call()

	parsed := req.(map[string]interface{})["state"]

	WalletStatus[cur] = false

	if parsed != nil {
		if strings.EqualFold(parsed.(string), "online") {
			WalletStatus[cur] = true
		}
	}
	return WalletStatus[cur]
}

// GetSymbols ...
func GetSymbols () (symbols []string) {

	u, err := url.Parse(BaseUrl + "/webapi/v3/markets.json")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"Content-Type": "application/json"},
		Body:    nil,
	}

	req := r.Call()

	for _, i := range req.([]interface{}) {
		d := i.(map[string]interface{})
		s := pkg.Symbol{
			Coin:     strings.Split(d["name"].(string), "/")[0],
			Currency: strings.Split(d["name"].(string), "/")[1],
		}

		sym, err := s.SymbolConverter()

		pkg.ErrorCheck(err)

		if GetWalletStatus(s.Coin) && GetWalletStatus(s.Currency) {
			symbols = append(symbols, sym.(string))
		}
	}
	return symbols
}