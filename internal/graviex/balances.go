package graviex

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// GetBalances ...
func GetBalances (key, secret string) (map[string]float64) {

	balances := make(map[string]float64)

	endoint := "/webapi/v3/members/me.json"

	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)

	queryString := fmt.Sprintf("access_key=%s&tonce=%s", key, timestamp)

	preparedString := fmt.Sprintf("GET|%s|%s", endoint, queryString)

	sign := pkg.SignWithSecret(preparedString, secret, "sha256")

	u, err := url.Parse(fmt.Sprintf("%s%s?%s&signature=%s", BaseUrl, endoint, queryString, sign))

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"Content-Type": "application/json"},
		Body:    nil,
	}

	req := r.Call()

	for _, v := range req.(map[string]interface{})["accounts_filtered"].([]interface{}) {
		d := v.(map[string]interface{})
		cur := strings.ToUpper(d["currency"].(string))
		bal, err := strconv.ParseFloat( d["balance"].(string), 64)
		pkg.ErrorCheck(err)
		balances[cur] = bal
	}

	return balances
}