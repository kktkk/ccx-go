package graviex

import (
	"ccx/pkg"
	"fmt"
	"log"
	"net/url"
	"strings"
	"time"
)

// TODO needs testing to parse the responses

// CreateOrder ...
func CreateOrder(key, secret, side, symbol string, price, amount float64) string {

	endoint := "/webapi/v3/orders/me.json"

	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)

	queryString := fmt.Sprintf(
		"access_key=%s&market=%sprice=%f&side=%s&tonce=%s&volume=%f", 
		key, 
		strings.ToLower(strings.Replace(symbol, "/", "", 1)), 
		price, 
		strings.ToLower(side), 
		timestamp, 
		amount,
	)

	preparedString := fmt.Sprintf("POST|%s|%s", endoint, queryString)

	sign := pkg.SignWithSecret(preparedString, secret, "sha256")

	u , err := url.Parse(fmt.Sprintf("%s%s?%s&signature=%s", BaseUrl, endoint, queryString, sign))

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{"Content-Type":"application/json"},
		Body: nil,
	}

	req := r.Call()

	log.Println(req)

	return ""
}

// CheckOrder ...
func CheckOrder(key, secret, symbol, orderId string) interface{} {
	endoint := "/webapi/v3/order.json"

	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)

	queryString := fmt.Sprintf(
		"access_key=%s&id=%s&tonce=%s", 
		key, 
		orderId, 
		timestamp, 
	)

	preparedString := fmt.Sprintf("GET|%s|%s", endoint, queryString)

	sign := pkg.SignWithSecret(preparedString, secret, "sha256")

	u , err := url.Parse(fmt.Sprintf("%s%s?%s&signature=%s", BaseUrl, endoint, queryString, sign))

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"Content-Type":"application/json"},
		Body: nil,
	}

	req := r.Call()

	log.Println(req)

	return req
}

// CancelOrder ...
func CancelOrder(key, secret, symbol, orderId string) bool {
	endoint := "/webapi/v3/order/delete.json"

	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)

	queryString := fmt.Sprintf(
		"access_key=%s&id=%s&tonce=%s", 
		key, 
		orderId, 
		timestamp, 
	)

	preparedString := fmt.Sprintf("POST|%s|%s", endoint, queryString)

	sign := pkg.SignWithSecret(preparedString, secret, "sha256")

	u , err := url.Parse(fmt.Sprintf("%s%s?%s&signature=%s", BaseUrl, endoint, queryString, sign))

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{"Content-Type":"application/json"},
		Body: nil,
	}

	req := r.Call()

	log.Println(req)

	return false
}