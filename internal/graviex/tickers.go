package graviex

import (
	"ccx/pkg"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers () (tickers []pkg.Ticker) {

	u, err := url.Parse(BaseUrl + "/webapi/v3/tickers.json")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{"Content-Type": "application/json"},
		Body:   nil,
	}

	req := r.Call()

	data := req.(map[string]interface{}) 

	for _, i := range data {
		d := i.(map[string]interface{})

		s := pkg.Symbol{
			Coin:     strings.ToUpper(d["base_unit"].(string)),
			Currency: strings.ToUpper(d["quote_unit"].(string)),
		}

		if !GetWalletStatus(s.Coin) || !GetWalletStatus(s.Currency) {
			continue
		}

		bid, _ := strconv.ParseFloat(d["buy"].(string), 64)
		ask, _ := strconv.ParseFloat(d["sell"].(string), 64)
		baseVol, _ := strconv.ParseFloat(d["volume"].(string), 64)
		quoteVol, _ := strconv.ParseFloat(d["volume2"].(string), 64)

		t := pkg.Ticker{
			Symbol: s,
			BidPrice: bid,
			BidQty: 0.0,
			AskPrice: ask,
			AskQty: 0.0,
			BaseVolume: baseVol,
			QuoteVolume: quoteVol,
			Exchange: "graviex",
			Timestamp: int(time.Now().Unix()),
		}

		tickers = append(tickers, t)
	}

	return tickers
}