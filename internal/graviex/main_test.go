package graviex_test

import (
	"ccx/internal/graviex"
	"log"
	"testing"
)

var key = ""
var secret = ""

func TestGetSymbols (t *testing.T) {
	s := graviex.GetSymbols()
	log.Println(s)
	if len(s) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(s))
	}
}

func TestGetTickers (t *testing.T) {
	tickers := graviex.GetTickers()
	log.Println(tickers)
	if len(tickers) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(tickers) )
	}
}

func TestGetBalances (t *testing.T) {
	bals := graviex.GetBalances(key, secret)
	log.Println(bals)
	if len(bals) == 0 {
		t.Errorf("Expected lenght of balances bigger than 0, got %v", len(bals) )
	}
}

func TestCreateOrder (t *testing.T) {
	order := graviex.CreateOrder(key, secret, "BTC/USD", "buy", 30000, 0.1)
	log.Println(order)
}