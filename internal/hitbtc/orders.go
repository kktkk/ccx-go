package hitbtc

import (
	"ccx/pkg"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strings"
	"time"
)

// CreateOrder ...
func CreateOrder(key, secret, side, symbol string, price, amount float64) (string) {
	
	endoint := "/api/3/spot/order"

	u, err := url.Parse(BaseUrl + endoint)

	pkg.ErrorCheck(err)
	
	body := map[string]interface{}{
		"symbol": strings.Replace(symbol, "/", "",1),
		"side": side,
		"type": "limit",
		"time_in_force": "GTC",
		"quantity": fmt.Sprintf("%f",amount),
		"price": fmt.Sprintf("%f",price),
	}

	bodyJ, err := json.Marshal(body)

	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000

	preparedString := fmt.Sprintf("POST%s%s%d", endoint, string(bodyJ), timestamp)

	signature := pkg.SignWithSecret(preparedString, secret, "sha256")

	preparedHeader := fmt.Sprintf("%s:%s:%d", key, signature, timestamp)

	encoded := base64.StdEncoding.EncodeToString([]byte(preparedHeader))

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"Authorization": fmt.Sprintf("HS256 %s", encoded),
		},
		Body:    body,
	}

	data := r.Call()

	log.Println(data)

	return data.(map[string]interface{})["id"].(string)
}

// CheckOrder ...
func CheckOrder(key, secret, symbol, orderId string) interface{} {

	endoint := "/api/3/spot/order/" + orderId

	u , err := url.Parse(BaseUrl + endoint)

	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000

	preparedString := fmt.Sprintf("GET%s%d", endoint, timestamp)

	signature := pkg.SignWithSecret(preparedString, secret, "sha256")

	preparedHeader := fmt.Sprintf("%s:%s:%d", key, signature, timestamp)

	encoded := base64.StdEncoding.EncodeToString([]byte(preparedHeader))

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers:map[string]string{
			"Content-Type": "application/json",
			"Authorization": fmt.Sprintf("HS256 %s", encoded),
		},
		Body:    nil,
	}

	data := r.Call()

	return data
}

// CancelOrder ...
func CancelOrder(key, secret, symbol, orderId string) bool {
	
	endoint := "/api/3/spot/order/" + orderId

	u , err := url.Parse(BaseUrl + endoint)

	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000

	preparedString := fmt.Sprintf("DELETE%s%d", endoint, timestamp)

	signature := pkg.SignWithSecret(preparedString, secret, "sha256")

	preparedHeader := fmt.Sprintf("%s:%s:%d", key, signature, timestamp)

	encoded := base64.StdEncoding.EncodeToString([]byte(preparedHeader))

	r := pkg.Request{
		Method:  "delete",
		URL:     u,
		Headers:map[string]string{
			"Content-Type": "application/json",
			"Authorization": fmt.Sprintf("HS256 %s", encoded),
		},
		Body:    nil,
	}

	data := r.Call()

	return data.(map[string]interface{})["status"].(string) == "canceled"
}