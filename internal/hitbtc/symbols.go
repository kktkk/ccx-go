package hitbtc

import (
	"ccx/pkg"
	"net/url"
)

// WalletStatus ...
func WalletStatus () (map[string]bool) {

	walletStatus := make(map[string]bool)

	u, err := url.Parse(BaseUrl + "/api/3/public/currency")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	data := r.Call()

	for k, v := range data.(map[string]interface{}){
		if v.(map[string]interface{})["payin_enabled"].(bool) && v.(map[string]interface{})["payout_enabled"].(bool) {
			walletStatus[k] = true
			} else {
			walletStatus[k] = false
		}
	}
	return walletStatus
}

// GetSymbols ...
func GetSymbols () (symbols []string) {

	u, err := url.Parse(BaseUrl + "/api/3/public/symbol")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}
	
	data := r.Call()

	walletStatus := WalletStatus()

	for _, i := range data.(map[string]interface{}){

		d := i.(map[string]interface{})
		
		if d["type"] == "spot" {
			s := pkg.Symbol{
				Coin:     d["base_currency"].(string),
				Currency: d["quote_currency"].(string),
			}
			if !walletStatus[s.Coin] || !walletStatus[s.Currency] {
				continue
			}
			sym, err := s.SymbolConverter()
			pkg.ErrorCheck(err)
			symbols = append(symbols, sym.(string))
		}

	}

	return symbols

}