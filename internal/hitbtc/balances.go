package hitbtc

import (
	"ccx/pkg"
	"encoding/base64"
	"fmt"
	"net/url"
	"strconv"
	"time"
)

// GetBalances ...
func GetBalances(key, secret string) (map[string]float64) {

	balances := make(map[string]float64)

	endoint := "/api/3/spot/balance"

	u, err := url.Parse(BaseUrl + endoint)

	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000

	preparedString := fmt.Sprintf("GET%s%d", endoint, timestamp)

	signature := pkg.SignWithSecret(preparedString, secret, "sha256")

	preparedHeader := fmt.Sprintf("%s:%s:%d", key, signature, timestamp)

	encoded := base64.StdEncoding.EncodeToString([]byte(preparedHeader))

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
			"Authorization": fmt.Sprintf("HS256 %s", encoded),
		},
		Body:    nil,
	}

	data := r.Call()
	
	for _, obj := range data.([]interface{}) {
		d := obj.(map[string]interface{})
		bal, err := strconv.ParseFloat(d["available"].(string), 64)
		pkg.ErrorCheck(err)
		balances[d["currency"].(string)] = bal
	}

	return balances
}