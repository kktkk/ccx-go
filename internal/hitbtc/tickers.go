package hitbtc

import (
	"ccx/pkg"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers() (tickers []pkg.Ticker) {

	u, err := url.Parse(BaseUrl + "/api/2/public/ticker")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}
	
	data := r.Call()

	parsed := data.([]interface{})

	walletStatus := WalletStatus()
	symbols := GetSymbols()

	for _, i := range parsed {

		var coin, cur string
		reparsed := i.(map[string]interface{})
		for _, s := range symbols {
			symbolCoin := strings.Split(s, "/")[0]
			symbolCur := strings.Split(s, "/")[1]
			if strings.Contains(reparsed["symbol"].(string), symbolCoin) {
				coin = symbolCoin
				cur = strings.Replace(reparsed["symbol"].(string), symbolCoin, "", 1)
			} else if strings.Contains(reparsed["symbol"].(string), symbolCur) {
				coin = strings.Replace(reparsed["symbol"].(string), symbolCur, "", 1)
				cur = symbolCur
			}
		}

		if !walletStatus[cur] || !walletStatus[coin] {
			continue
		}
		
		var bidPrice float64
		var askPrice float64

		if reparsed["bid"] == nil {
			bidPrice = 0.0
		} else {
			bidPrice, _ = strconv.ParseFloat(reparsed["bid"].(string), 64)
		}

		if reparsed["ask"] == nil {
			askPrice = 0.0
		} else {
			askPrice, _ = strconv.ParseFloat(reparsed["ask"].(string), 64)
		}
		
		baseVol, _ := strconv.ParseFloat(reparsed["volume"].(string), 64)
		quoteVol, _ := strconv.ParseFloat(reparsed["volumeQuote"].(string), 64)

		tickers = append(tickers, pkg.Ticker{
			Symbol: pkg.Symbol{Coin: coin, Currency: cur},
			BidPrice: bidPrice,
			BidQty: 0.0,
			AskPrice: askPrice,
			AskQty: 0.0,
			BaseVolume: baseVol,
			QuoteVolume: quoteVol,
			Exchange: "hitbtc",
			Timestamp: int(time.Now().Unix())})
	}
	if len(tickers) == 0 {
		log.Println("Could not get tickers from hitbtc.")
	}
	return tickers
}
