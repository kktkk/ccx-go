package stakecube

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strconv"
	"time"
)

// GetOrderbook ...
func GetOrderbook (k string) (book []float64) {
	counter := 0
	u, err := url.Parse(fmt.Sprintf("%s/api/v2/exchange/spot/orderbook?market=%s", BaseUrl, k))
	pkg.ErrorCheck(err)
	r:= pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	req := r.Call()
	success := req.(map[string]interface{})["success"]
	ask := 0.0
	askQty := 0.0
	bid := 0.0
	bidQty := 0.0
	if success == true {
		rsl := req.(map[string]interface{})["result"].(map[string]interface{})
		for k, v := range rsl {
			if k == "asks" {
				ask, _ = strconv.ParseFloat(v.([]interface{})[0].(map[string]interface{})["price"].(string), 64)
				askQty, _ = strconv.ParseFloat(v.([]interface{})[0].(map[string]interface{})["amount"].(string), 64)
			} else if k == "bids" {
				bid, _ = strconv.ParseFloat(v.([]interface{})[0].(map[string]interface{})["price"].(string), 64)
				bidQty, _ = strconv.ParseFloat(v.([]interface{})[0].(map[string]interface{})["amount"].(string), 64)
			}
		}
		book = []float64{ask, askQty, bid, bidQty}
	} else {
		if counter < 2 {
			time.Sleep(500 * time.Millisecond)
			GetOrderbook(k)
			counter++
		}
	}
	return book
}