package stakecube

import (
	"fmt"
	"log"
	"time"
)

// GetBalances ...
func GetBalances (key, secret string) (map[string]interface{}) {

	balances := make(map[string]interface{})

	url := BaseUrl + "/api/v2/user/account?"

	timestamp := time.Now().Unix() * 1000

	body := fmt.Sprintf("nonce=%d" , timestamp)

	signature := Sign(body, secret)

	url = fmt.Sprintf("%s%s&signature=%s", url, body, signature)

	req := MakeAuthRequest("get", url, key, nil)

	log.Println(req)

	return balances
}