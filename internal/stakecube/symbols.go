package stakecube

import (
	"ccx/pkg"
	"net/url"
	"strings"
)

// GetSymbols ...
func GetSymbols () (symbols []string) {

	u, err := url.Parse(BaseUrl + "/api/v2/exchange/spot/markets")
	pkg.ErrorCheck(err)
	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	data := r.Call()

	parsed := data.(map[string]interface{})["result"]

	reparsed := parsed.(map[string]interface{})

	for k := range reparsed {
		s := strings.Replace(k, "_", "/", 1)
		symbols = append(symbols, s)
	}

	return symbols
}