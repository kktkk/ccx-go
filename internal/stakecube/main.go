package stakecube

import (
	"bytes"
	"ccx/pkg"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"
)

// BaseUrl ...
var BaseUrl = "https://stakecube.io"


// Sign ...
func Sign(data, secret string) (string) {
	hash := hmac.New(sha256.New, []byte(secret))
	hash.Write([]byte(data))
	sha := hex.EncodeToString(hash.Sum(nil))
	return sha
}

// MakeAuthRequest ...
func MakeAuthRequest (method, url, key string, data []byte ) (rsl interface{}) {
	
	client := &http.Client{}

	var req *http.Request
	var err error

	if data == nil {
		req, err = http.NewRequest(strings.ToUpper(method), url, nil)
	} else {
		req, err = http.NewRequest(strings.ToUpper(method), url, bytes.NewBuffer(data))
	}
	
	pkg.ErrorCheck(err)

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("X-API-KEY", key)

	log.Println(req)

	resp, err := client.Do(req)
	
	log.Println(resp)

	req.Close = true

	if resp.StatusCode == 429 {
		time.Sleep(2 * time.Second)
		MakeAuthRequest(method, url, key, data)
	}

	pkg.ErrorCheck(err)

	defer resp.Body.Close()
	
	json.NewDecoder(resp.Body).Decode(&rsl)

	return rsl
}