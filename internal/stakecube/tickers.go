package stakecube

import (
	"ccx/pkg"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers () (tickers []pkg.Ticker) {
	
	u, err := url.Parse(BaseUrl + "/api/v2/exchange/spot/markets?orderData=true&priceHistory=false")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:    	u,
		Headers: map[string]string{},
		Body:    nil,
	}
	data :=r.Call()

	parsed := data.(map[string]interface{})["result"]

	for k, v := range parsed.(map[string]interface{}) {
		
		if v.(map[string]interface{})["state"].(string) == "ACTIVE" {

			coin := strings.Split(k, "_")[0]
			cur := strings.Split(k, "_")[1]
	
			d := v.(map[string]interface{})

			bidQty := 0.0
			askQty := 0.0
	
			baseVol, ok := d["volumeBase24h"].(float64)
			 if !ok {
				 baseVol, _ = strconv.ParseFloat(d["volumeBase24h"].(string), 64)
			 }
			 
			quoteVol, ok := d["volumeTrade24h"].(float64)
			if !ok {
				quoteVol, _ = strconv.ParseFloat(d["volumeTrade24h"].(string), 64)
			}
	
			bid, ok := d["bestBid"].(float64)
			if !ok {
				quoteVol, _ = strconv.ParseFloat(d["bestBid"].(string), 64)
			}
	
			ask, ok := d["bestAsk"].(float64)
			if !ok {
				quoteVol, _ = strconv.ParseFloat(d["bestAsk"].(string), 64)
			}

			if len(d["bids"].([]interface{})) > 0 {
				for _, v := range d["bids"].([]interface{})[0].(map[string]interface{}) {
					p := v.(map[string]interface{})
					bidQty, _ = strconv.ParseFloat(p["amount"].(string), 64)
				}
			}

			if len(d["asks"].([]interface{})) > 0 {
				for _, v := range d["asks"].([]interface{})[0].(map[string]interface{}) {
					p := v.(map[string]interface{})
					askQty, _ = strconv.ParseFloat(p["amount"].(string), 64)
				}
			}
	
			new_ticker := pkg.Ticker {
				Symbol: pkg.Symbol{
					Coin:     coin,
					Currency: cur,
				},
				BidPrice: bid,
				BidQty: bidQty,
				AskPrice: ask,
				AskQty: askQty,
				BaseVolume: baseVol,
				QuoteVolume: quoteVol,
				Exchange: "stakecube",
				Timestamp: int(time.Now().Unix()),
			}
			
			tickers = append(tickers, new_ticker)
		}	
	}
	return tickers
}