package poloniex

import (
	"ccx/pkg"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers () (tickers []pkg.Ticker) {

	u, err := url.Parse(BaseUrl + "?command=returnTicker")
	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	data := r.Call()

	parsed := data.(map[string]interface{})

	var coin string
	var cur string

	walletStatus := WalletStatus()

	for k, v := range parsed {
		coin = strings.Split(k, "_")[1]
		cur = strings.Split(k, "_")[0]

		if walletStatus[coin] && walletStatus[cur] {
			bp, _ := strconv.ParseFloat(v.(map[string]interface{})["highestBid"].(string), 64)
			ap, _ := strconv.ParseFloat(v.(map[string]interface{})["lowestAsk"].(string), 64)
			baseVol, _ := strconv.ParseFloat(v.(map[string]interface{})["baseVolume"].(string), 64)
			quoteVol, _ := strconv.ParseFloat(v.(map[string]interface{})["quoteVolume"].(string), 64)
			
			tickers = append(tickers, pkg.Ticker{
				Symbol: pkg.Symbol{
					Coin:     coin,
					Currency: cur,
				},
				BidPrice: bp,
				BidQty: 0.0,
				AskPrice: ap,
				AskQty: 0.0,
				BaseVolume: baseVol,
				QuoteVolume: quoteVol,
				Exchange: "poloniex",
				Timestamp: int(time.Now().Unix())})
		}
	}
	return tickers
}