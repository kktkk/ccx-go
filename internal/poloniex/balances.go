package poloniex

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strconv"
	"time"
)

// GetBalances...
func GetBalances (key, secret string) (balances map[string]float64) {

	u, err := url.Parse(TradingUrl)
	
	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000000

	q := url.Values{}
	q.Add("command", "returnBalances")
	q.Add("nonce", fmt.Sprintf("%d", timestamp))

	pkg.ErrorCheck(err)

	signature := pkg.SignWithSecret(q.Encode(), secret, "sha512")
	
	r:= pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Key": key,
			"Sign": signature,
			"Content-Type": "application/x-www-form-urlencoded",
		},
		Body:    q.Encode(),
	}

	data := r.Call()

	balances = make(map[string]float64)

	for k, v := range data.(map[string]interface{}) {
		bal, err := strconv.ParseFloat(v.(string), 64)
		pkg.ErrorCheck(err)
		if bal > 0 {
			balances[k] = bal
		}
	}
	return balances
}