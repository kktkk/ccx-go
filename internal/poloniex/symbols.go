package poloniex

import (
	"ccx/pkg"
	"net/url"
	"strings"
)

// WalletStatus ...
func WalletStatus () (map[string]bool) {
	walletStatus := make(map[string]bool)
	u, err := url.Parse(BaseUrl + "?command=returnCurrencies")
	pkg.ErrorCheck(err)
	r:= pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}
	data := r.Call()
	for k, v := range data.(map[string]interface{}){
		d := v.(map[string]interface{})
		if d["disabled"].(float64) == 0 && d["frozen"].(float64) == 0 {
			walletStatus[k] = true
		} else {
			walletStatus[k] = false
		}
	}
	return walletStatus
}

// GetSymbols ...
func GetSymbols () (symbols []string) {

	u, err := url.Parse(BaseUrl + "?command=returnTicker")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	data := r.Call()

	parsed := data.(map[string]interface{})

	walletStatus := WalletStatus()

	for k := range parsed {
		coin := strings.Split(k, "_")[0]
		cur := strings.Split(k, "_")[1]
		if walletStatus[coin] && walletStatus[cur] {
			s := strings.Replace(k, "_", "/", 1)
			symbols = append(symbols, s)
		}
	}

	return symbols
}