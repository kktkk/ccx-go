package poloniex_test

import (
	"ccx/internal/poloniex"
	"log"
	"testing"
)

var key = ""
var secret = ""

func TestGetSymbols (t *testing.T) {
	s := poloniex.GetSymbols()
	log.Println(s)
	if len(s) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(s))
	}
}

func TestGetTickers (t *testing.T) {
	tickers := poloniex.GetTickers()
	log.Println(tickers)
	if len(tickers) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(tickers) )
	}
}


func TestGetBalances (t *testing.T) {
	bals := poloniex.GetBalances(key, secret)
	log.Println(bals)
	if len(bals) == 0 {
		t.Errorf("Expected lenght of balances bigger than 0, got %v", len(bals) )
	}
}

var currentOrder string

func TestCreateOrder (t *testing.T) {
	orderId := poloniex.CreateOrder(key, secret, "buy", "BTC/USDT", 35000, 1)
	log.Println(orderId)
	currentOrder = orderId
	if len(orderId) == 0{
		t.Errorf("Expected lenght of orderId bigger than 0, got %v", 0 )
	}
}

func TestCheckOrder (t *testing.T) {
	order := poloniex.CheckOrder(key, secret, "", currentOrder)
	log.Println(order)
	if order == nil{
		t.Errorf("Expected order to be different than nil, got %v", nil )
	}
}

func TestCancelOrder (t *testing.T) {
	order := poloniex.CancelOrder(key, secret, "", currentOrder)
	log.Println(order)
	if !order {
		t.Errorf("Expected order to be true , got %v", false )
	}
}