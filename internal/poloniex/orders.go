package poloniex

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strings"
	"time"
)

// CreateOrder ...
func CreateOrder(key, secret, side, symbol string, price, amount float64) (string) {
	
	u, err := url.Parse(TradingUrl)
	
	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000000

	q := url.Values{}
	q.Add("command", strings.ToLower(side))
	q.Add("currencyPair", strings.Replace(symbol, "/", "_", 1))
	q.Add("rate", fmt.Sprintf("%f", price))
	q.Add("amount", fmt.Sprintf("%f", amount))
	q.Add("nonce", fmt.Sprintf("%d", timestamp))

	pkg.ErrorCheck(err)

	signature := pkg.SignWithSecret(q.Encode(), secret, "sha512")
	
	r:= pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Key": key,
			"Sign": signature,
			"Content-Type": "application/x-www-form-urlencoded",
		},
		Body:    q.Encode(),
	}

	data := r.Call()
	
	return data.(map[string]interface{})["orderNumber"].(string)
}

// CheckOrder ...
func CheckOrder(key, secret, symbol, orderId string) interface{} {
	
	u, err := url.Parse(TradingUrl)
	
	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000000

	q := url.Values{}
	q.Add("command", "returnOrderStatus")
	q.Add("orderNumber", orderId)
	q.Add("nonce", fmt.Sprintf("%d", timestamp))

	pkg.ErrorCheck(err)

	signature := pkg.SignWithSecret(q.Encode(), secret, "sha512")
	
	r:= pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Key": key,
			"Sign": signature,
			"Content-Type": "application/x-www-form-urlencoded",
		},
		Body:    q.Encode(),
	}

	data := r.Call()

	return data.(map[string]interface{})["result"].(map[string]interface{})[orderId]
} 

// CancelOrder ...
func CancelOrder (key, secret, symbol, orderId string) bool {
		
	u, err := url.Parse(TradingUrl)
	
	pkg.ErrorCheck(err)

	timestamp := time.Now().Unix() * 1000000

	q := url.Values{}
	q.Add("command", "cancelOrder")
	q.Add("orderNumber", orderId)
	q.Add("nonce", fmt.Sprintf("%d", timestamp))

	pkg.ErrorCheck(err)

	signature := pkg.SignWithSecret(q.Encode(), secret, "sha512")
	
	r:= pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Key": key,
			"Sign": signature,
			"Content-Type": "application/x-www-form-urlencoded",
		},
		Body:    q.Encode(),
	}

	data := r.Call()

	return data.(map[string]interface{})["sucess"].(int) == 1
}