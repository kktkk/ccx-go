package cryptocom

import (
	"ccx/pkg"
	"net/url"
	"strings"
)

// GetSymbols ...
func GetSymbols () (symbols []string) {

	u, err := url.Parse(BaseUrl + "/public/get-instruments")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	req := r.Call()

	data := req.(map[string]interface{})["result"].(map[string]interface{})["instruments"]

	for _, i := range data.([]interface{}) {
		s := i.(map[string]interface{})["instrument_name"]
		sym := pkg.Symbol{
			Coin:     strings.Split(s.(string), "_")[0],
			Currency: strings.Split(s.(string), "_")[1],
		}

		symb, err := sym.SymbolConverter()
		
		pkg.ErrorCheck(err)
		
		symbols = append(symbols, symb.(string))
	}
	return symbols
}