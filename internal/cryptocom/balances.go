package cryptocom

import (
	"ccx/pkg"
	"fmt"
	"log"
	"net/url"
	"time"
)

// GetBalances ...
func GetBalances (key, secret string) (map[string]float64) {
	
	balances := make(map[string]float64)
	
	timestamp := time.Now().Unix() * 1000
	
	method := "private/get-account-summary"
	
	u, err := url.Parse(fmt.Sprintf("%s/%s", BaseUrl, method))

	pkg.ErrorCheck(err)

	params := make(map[string]interface{})

	body := map[string]interface{}{"api_key": key, "nonce": timestamp, "method": method, "params": params}
	
	paramStr :=  fmt.Sprintf("%s%s%s%d", method, key, params, timestamp)

	pkg.ErrorCheck(err)
	
	sign := pkg.SignWithSecret(paramStr, secret, "sha256")

	body["sig"] = sign

	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body:   body,
	}

	req :=r.Call()

	log.Println(req)

	return balances
}