package cryptocom

import (
	"ccx/pkg"
	"net/url"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers () (tickers []pkg.Ticker) {

	u, err := url.Parse(BaseUrl + "/public/get-ticker")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    map[string]interface{}{},
	}

	raw := r.Call()

	data := raw.(map[string]interface{})["result"].(map[string]interface{})["data"]

	for _, i := range data.([]interface{}) {
		coin := strings.Split(i.(map[string]interface{})["i"].(string), "_")[0]
		cur := strings.Split(i.(map[string]interface{})["i"].(string), "_")[1]

		t := pkg.Ticker {
			Symbol: pkg.Symbol{
				Coin:     coin,
				Currency: cur,
			},
			BidPrice: i.(map[string]interface{})["b"].(float64),
			BidQty: 0.0,
			AskPrice: i.(map[string]interface{})["a"].(float64),
			AskQty: 0.0,
			BaseVolume: i.(map[string]interface{})["v"].(float64),
			QuoteVolume: 0.0,
			Exchange: "crypto.com",
			Timestamp: int(time.Now().Unix()),
		}
		tickers = append(tickers, t)
	}
	return tickers
}