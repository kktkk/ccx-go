package cryptocom

import (
	"ccx/pkg"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"
)

// BaseUrl ...
var BaseUrl = "https://api.crypto.com/v2"

// Sign ...
func Sign(data string) (string) {
	encode := sha256.New()
	encode.Write([]byte(data))
	sign := hex.EncodeToString(encode.Sum(nil))
	return sign
}

// MakeAuthRequest ...
func MakeAuthRequest (method, url, data string ) (rsl interface{}) {
	client := &http.Client{}

	var req *http.Request

	if len(data) == 0 {
		req, _ = http.NewRequest(strings.ToUpper(method), url, nil)
	} else {
		req, _ = http.NewRequest(strings.ToUpper(method), url, strings.NewReader(data))
	}

	req.Header.Set("Content-Type", "application/json")

	resp, _ := client.Do(req)

	log.Println(resp)

	req.Close = true

	if resp.StatusCode == 429 {
		time.Sleep(2 * time.Second)
		MakeAuthRequest(method, url, data)
	}

	defer resp.Body.Close()
	
	json.NewDecoder(resp.Body).Decode(&rsl)

	return rsl
}

// ParamString ...
func ParamString(param map[string]interface{}) (string, error) {

	if param == nil {
		return "", errors.New("parameter cannot be nil")
	}

	// Sorted Parameters
	keys := []string{}
	rslStr := ""
	for key := range param {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, value := range keys {
		v := param[value]
		paramStr, err := json.Marshal(v)
		pkg.ErrorCheck(err)
		rslStr += (value + string(paramStr))
	}

	return rslStr, nil
}
