package cryptocom_test

import (
	"ccx/internal/cryptocom"
	"log"
	"testing"
)

// TODO remove testing keys
var key = ""
var secret = ""

// func TestGetSymbols (t *testing.T) {
// 	s := cryptocom.GetSymbols()
// 	log.Println(s)
// 	if len(s) == 0 {
// 		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(s))
// 	}
// }

// func TestGetTickers (t *testing.T) {
// 	tickers := cryptocom.GetTickers()
// 	log.Println(tickers)
// 	if len(tickers) == 0 {
// 		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(tickers) )
// 	}
// }

func TestGetBalances (t *testing.T) {
	balances := cryptocom.GetBalances(key, secret)
	log.Println(balances)
	if len(balances) == 0 {
		t.Errorf("Expected lenght of balances bigger than 0, got %v", len(balances) )
	}
}