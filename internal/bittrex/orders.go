package bittrex

import (
	"ccx/pkg"
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strings"
	"time"
)

// CreateOrder ...
func CreateOrder(key, secret, side, symbol string, price, amount float64) (string) {
	// {
	// 	"marketSymbol": "string",
	// 	"direction": "string",
	// 	"type": "string",
	// 	"quantity": "number (double)",
	// 	"ceiling": "number (double)",
	// 	"limit": "number (double)",
	// 	"timeInForce": "string",
	// 	"clientOrderId": "string (uuid)",
	// 	"useAwards": "boolean"
	// }

	u, err := url.Parse(fmt.Sprintf("%s/v3/orders", BaseUrl))

	pkg.ErrorCheck(err)

	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)

	pair := strings.Replace(symbol, "/", "-", 1)

	body := map[string]interface{}{
		"marketSymbol": pair,
		"direction": strings.ToUpper(side),
		"type":"LIMIT",
		"quantity": fmt.Sprintf("%f", amount),
		"limit": fmt.Sprintf("%f", price),
		"timeInForce": "GOOD_TIL_CANCELLED",
	}
	
	jBody, err := json.Marshal(body)

	pkg.ErrorCheck(err)
	
	contentHash := createContentHash(string(jBody))

	preSign := timestamp + u.String() + "POST" + string(contentHash)

	signature := pkg.SignWithSecret(preSign, secret, "sha512")
	
	r := pkg.Request{
		Method:  "post",
		URL:     u,
		Headers: map[string]string{
			"Api-Key": key,
			"Api-Timestamp": timestamp,
			"Api-Content-Hash": contentHash,
			"Api-Signature": signature,
			"Content-Type": "application/json",
		},
		Body:    body,
	}

	rsl := r.Call()

	parsed := rsl.(map[string]interface{})

	if parsed["id"] == nil {
		return ""
	}
	
	return parsed["id"].(string)
}

// CheckOrder ...
func CheckOrder(key, secret, symbol, orderId string) interface{} {

	if len(orderId) == 0 {
		log.Fatalln("You need to provide an orderId")
	}
	
	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)
	
	u, err := url.Parse(fmt.Sprintf("%s/v3/orders/%s", BaseUrl, orderId))
	
	contentHash := createContentHash("")

	preSign := timestamp + u.String() + "GET" + string(contentHash)

	signature := pkg.SignWithSecret(preSign, secret, "sha512")

	pkg.ErrorCheck(err)
	
	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{
			"Api-Key": key,
			"Api-Timestamp": timestamp,
			"Api-Content-Hash": contentHash,
			"Api-Signature": signature,
			"Content-Type": "application/json",
		},
		Body:    nil,
	}
	rsl := r.Call()
	return rsl
}

// CancelOrder ...
func CancelOrder(key, secret, symbol, orderId string) (bool) {

	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)
	
	u, err := url.Parse(fmt.Sprintf("%s/v3/orders/%s", BaseUrl, orderId))
	
	contentHash := createContentHash("")

	preSign := timestamp + u.String() + "DELETE" + string(contentHash)

	signature := pkg.SignWithSecret(preSign, secret, "sha512")

	pkg.ErrorCheck(err)
	
	r := pkg.Request{
		Method:  "delete",
		URL:     u,
		Headers: map[string]string{
			"Api-Key": key,
			"Api-Timestamp": timestamp,
			"Api-Content-Hash": contentHash,
			"Api-Signature": signature,
			"Content-Type": "application/json",
		},
		Body:    nil,
	}
	rsl := r.Call()

	return strings.EqualFold(rsl.(map[string]interface{})["status"].(string), "CLOSED")
}
