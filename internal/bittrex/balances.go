package bittrex

import (
	"ccx/pkg"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"time"
)

// GetBalances ...
func GetBalances(key, secret string) (map[string]float64){

	balances := make(map[string]float64)
	
	u, err := url.Parse(BaseUrl + "/v3/balances")

	pkg.ErrorCheck(err)

	timestamp := fmt.Sprintf("%d", time.Now().Unix() * 1000)

	contentHash := createContentHash("")

	preSign := timestamp + u.String() + "GET" + string(contentHash)
	
	signature := pkg.SignWithSecret(preSign, secret, "sha512")

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{
			"Api-Key": key,
			"Api-Timestamp": timestamp,
			"Api-Content-Hash": contentHash,
			"Api-Signature": signature,
			"Content-Type": "application/json",
		},
		Body:    nil,
	}

	rsl := r.Call()

	log.Println(rsl)

	for _, i := range rsl.([]interface{}) {
		d := i.(map[string]interface{})
		bal, err := strconv.ParseFloat(d["available"].(string), 64)
		pkg.ErrorCheck(err)
		if bal > 0.0 {
			balances[d["currencySymbol"].(string)] = bal
		}
	}

	return balances
}