package bittrex

import (
	"ccx/pkg"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// GetTickers ...
func GetTickers() (tickers []pkg.Ticker) {

	u, err := url.Parse(BaseUrl + "/v3/markets/tickers")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body:    nil,
	}
	
	data := r.Call()

	parsed := data.([]interface{})

	volume := get24hData()

	wallet_status := WalletStatus()

	for _, i := range parsed {

		reparsed := i.(map[string]interface{})

		symbol := reparsed["symbol"].(string)
		
		s := pkg.Symbol{
			Coin:     strings.Split(symbol, "-")[0],
			Currency: strings.Split(symbol, "-")[1],
		}

		if !wallet_status[s.Coin] || !wallet_status[s.Currency] {
			continue
		}

		bidPrice, _ := strconv.ParseFloat(reparsed["bidRate"].(string), 64)
		askPrice, _ := strconv.ParseFloat(reparsed["askRate"].(string), 64)
		
		tickers = append(tickers, pkg.Ticker{
			Symbol: s,
			BidPrice: bidPrice,
			BidQty: 0.0,
			AskPrice: askPrice,
			AskQty: 0.0,
			BaseVolume: volume[symbol][0],
			QuoteVolume: volume[symbol][1],
			Exchange: "bittrex",
			Timestamp: int(time.Now().Unix())})
	}

	if len(tickers) == 0 {
		log.Println("Could not get tickers from bittrex.")
	}

	return tickers
}