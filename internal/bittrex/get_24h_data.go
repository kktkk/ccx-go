package bittrex

import (
	"ccx/pkg"
	"net/url"
	"strconv"
)

func get24hData () map[string][]float64 {

	u, err := url.Parse(BaseUrl + "/v3/markets/summaries")
	
	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	data := r.Call()

	parsed := data.([]interface{})

	var rsl = make(map[string][]float64)

	for _, i := range parsed {
		d := i.(map[string]interface{})
		baseVol, _ := strconv.ParseFloat(d["volume"].(string), 64)
		quoteVol, _ := strconv.ParseFloat(d["quoteVolume"].(string), 64)
		rsl[d["symbol"].(string)] = []float64{baseVol, quoteVol}
	}

	return rsl
}