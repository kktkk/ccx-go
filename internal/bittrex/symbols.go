package bittrex

import (
	"ccx/pkg"
	"net/url"
	"strings"
)

// WalletStatus...
func WalletStatus () (map[string]bool) {

	walletStatus := make(map[string]bool)

	u, err := url.Parse(BaseUrl + "/v3/currencies")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body:    nil,
	}

	data := r.Call()

	parsed := data.([]interface{})

	for _, i := range parsed {
		var rsl bool
		if strings.EqualFold(i.(map[string]interface{})["status"].(string), "online") {
			rsl = true
		} else {rsl = false}
		walletStatus[i.(map[string]interface{})["symbol"].(string)] = rsl
	}
	return walletStatus
}

// GetSymbols ...
func GetSymbols () (symbols []string) {
	
	u, err := url.Parse(BaseUrl + "/v3/markets")

	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body:    nil,
	}

	data := r.Call()

	parsed := data.([]interface{})

	walletStatus := WalletStatus()

	for _, i := range parsed {
		s := pkg.Symbol{
			Coin:     i.(map[string]interface{})["baseCurrencySymbol"].(string),
			Currency: i.(map[string]interface{})["quoteCurrencySymbol"].(string),
		} 
		if walletStatus[s.Coin] && walletStatus[s.Currency] {
			
			sym, err := s.SymbolConverter()
			
			pkg.ErrorCheck(err)
			
			symbols = append(symbols, sym.(string))
		}
	}
	return symbols
}