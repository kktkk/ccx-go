package bittrex

import (
	"crypto/sha512"
	"fmt"
)

// BaseUrl ...
var BaseUrl = "https://api.bittrex.com"

func createContentHash(data string) string {
	sha_512 := sha512.New()

	sha_512.Write([]byte(data))

	return fmt.Sprintf("%x", sha_512.Sum(nil))
}
