package bittrex_test

import (
	"ccx/internal/bittrex"
	"log"
	"testing"
)

var key = ""
var secret = ""

func TestGetSymbols (t *testing.T) {
	s := bittrex.GetSymbols()
	log.Println(s)
	if len(s) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(s))
	}
}

func TestGetTickers (t *testing.T) {
	s := bittrex.GetTickers()
	log.Println(s)
	if len(s) == 0 {
		t.Errorf("Expected lenght of symbols bigger than 0, got %v", len(s))
	}
}

func TestGetBalances (t *testing.T) {
	bal:= bittrex.GetBalances(key, secret)
	log.Println(bal)
	if len(bal) == 0 {
		t.Errorf("Expected lenght of balances bigger than 0, got %d", len(bal) )
	}
}

var currentOrder string

func TestCreateOrder (t *testing.T) {
	orderId:= bittrex.CreateOrder(key, secret, "sell", "BTC/USD", 55000, 0.001)
	log.Println(orderId)
	currentOrder = orderId
	if len(orderId) == 0 {
		t.Errorf("Expected lenght of orderId bigger than 0, got %d", len(orderId) )
	}
}

func TestCheckOrder (t *testing.T) {
	order:= bittrex.CheckOrder(key, secret, "BTC/USD", currentOrder)
	log.Println(order)
	o := order.(map[string]interface{})
	if len(o["id"].(string)) == 0 {
		t.Errorf("Expected order lenght to be greater than 0, got %d", len(o["id"].(string)))
	}
}

func TestCancelOrder (t *testing.T) {
	success:= bittrex.CancelOrder(key, secret, "BTC/USD", currentOrder)
	log.Println(success)
	if !success {
		t.Errorf("Expected cancel order result to be true, got %v", success )
	}
}