package btcpop

import (
	"ccx/pkg"
	"log"
	"net/url"
	"strconv"
	"time"
)

// GetTickers ...
func GetTickers() []pkg.Ticker {

	u, err := url.Parse("https://btcpop.co/api/market-public.php")
	
	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}
	
	data := r.Call()

	var tickers []pkg.Ticker
	
	parsed := data.([]interface{})
	
	for _, i := range parsed {
		
		reparsed := i.(map[string]interface{})

		if reparsed["buyPrice"] != nil && reparsed["sellPrice"] != nil{
			bidPrice, _ := strconv.ParseFloat(reparsed["buyPrice"].(string), 64)
			askPrice, _ := strconv.ParseFloat(reparsed["sellPrice"].(string), 64)
			
			tickers = append(tickers, pkg.Ticker{
				Symbol: pkg.Symbol{Coin: reparsed["ticker"].(string), Currency: "BTC"},
				BidPrice: bidPrice,
				BidQty: 0.0,
				AskPrice: askPrice,
				AskQty: 0.0,
				BaseVolume: 0.0,
				QuoteVolume: 0.0,
				Exchange: "btcpop",
				Timestamp: int(time.Now().Unix())})
		}
	}

	if len(tickers) == 0 {
		log.Println("Could not get tickers from btcpop.")
	}

	return tickers
}