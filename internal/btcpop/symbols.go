package btcpop

import (
	"ccx/pkg"
	"fmt"
	"net/url"
	"strings"
)

// GetSymbols ...
func GetSymbols() (symbols []string) {

	u, err := url.Parse("https://btcpop.co/api/market-public.php")
	
	pkg.ErrorCheck(err)

	r := pkg.Request{
		Method:  "get",
		URL:     u,
		Headers: map[string]string{},
		Body:    nil,
	}

	data := r.Call()
	
	parsed := data.([]interface{})
	
	for _, i := range parsed {
		
		reparsed := i.(map[string]interface{})

		if reparsed["buyPrice"] != nil && reparsed["sellPrice"] != nil{
			coin := reparsed["ticker"].(string)
			symbols = append(symbols, fmt.Sprintf("%s/BTC",
				strings.ToUpper(coin),
			))
		}
	}
			
	return symbols
}