package pkg

import (
	"fmt"
	"strings"
)

// SymbolConverter ...
func (s *Symbol) SymbolConverter () (symbol interface{}, err error) {
	if s.Coin == "" || s.Currency == "" {
		return nil, fmt.Errorf("error converting symbol: %v", s)
	}
	return strings.ToUpper(
		strings.Join(
			[]string{s.Coin, s.Currency}, "/",
	)), nil
}