package pkg

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

// MakeRequest function to make request and return a json result.
func MakeRequest(url string) interface{} {
	
	resp, err := http.Get(url)
	
	ErrorCheck(err)

	defer resp.Body.Close()

	var data interface{}

	body, err := ioutil.ReadAll(resp.Body)

	ErrorCheck(err)

	json.Unmarshal(body, &data)

	return data
}

// Call ...
func (r *Request) Call () (rsl interface{}) {

	client := &http.Client{}

	var req *http.Request
	var err error

	// We check if the method is GET so we can build query
	if strings.EqualFold(r.Method, "get") {
		
		req, err = http.NewRequest("GET", r.URL.String(), nil)

		ErrorCheck(err)
		
		if r.Body != nil {
			switch r.Body.(type) {
			case string:
				req.URL.RawQuery = r.Body.(string)
			case map[string]interface{}:
				req.URL.RawQuery = parseBodyQuery(r.Body.(map[string]interface{}))
			}
		}

		// Else body will be used in request body
	} else {

		if r.Body == nil {
			
			req, err = http.NewRequest(strings.ToUpper(r.Method), r.URL.String(), nil)
			
			ErrorCheck(err)

		} else {
			switch r.Body.(type) {
			case map[string]interface{}:
				b, err := json.Marshal(r.Body)		
				ErrorCheck(err)
				req, err = http.NewRequest(strings.ToUpper(r.Method), r.URL.String(), bytes.NewBuffer(b))
				ErrorCheck(err)
			case string:
				log.Println("r.Body IS A STRING:", r.Body)
				req, err = http.NewRequest(strings.ToUpper(r.Method), r.URL.String(), strings.NewReader(r.Body.(string)))
				ErrorCheck(err)
			}
		}
	}

	for k, v := range r.Headers {
		req.Header[k] = []string{v}
	}
	
	// TODO remove logs
	log.Println("REQUEST", req)

	resp, err := client.Do(req)

	log.Println("!!CALL RESPONSE!!", resp)
	
	ErrorCheck(err)

	Status429(resp.StatusCode, r.Call)

	req.Close = true

	defer resp.Body.Close()
	
	json.NewDecoder(resp.Body).Decode(&rsl)

	return rsl
}

func parseBodyQuery (body map[string]interface{}) string {
	query := url.Values{}
	for k, v := range body {
		query.Add(k, v.(string))
	}
	return query.Encode()
}