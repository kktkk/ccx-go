package pkg

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"hash"
)

// SignWithSecret ...
func SignWithSecret (data, secret, algo string) (string) {
	var h hash.Hash
	switch algo {
	case "sha256":
		h = hmac.New(sha256.New, []byte(secret))
	case "sha384":
		h = hmac.New(sha512.New384, []byte(secret))
	case "sha512":
		h = hmac.New(sha512.New, []byte(secret))
	}
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}

// SignWithoutSecret ...
func SignWithoutSecret (data, algo string) (string) {
	var h hash.Hash
	switch algo {
	case "sha256":
		h = sha256.New()
	case "sha384":
		h = sha512.New384()
	case "sha512":
		h = sha512.New()
	}
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}