package pkg

import "net/url"

// Request ...
type Request struct {
	Method string `json:"method"`
	URL *url.URL `json:"url"`
	Headers map[string]string `json:"headers"`
	Body interface{} `json:"body"`
}

// Response ...
type Response struct {
	Code int `json:"code"`
	Msg string `json:"msg"`
	Results []byte `json:"results"`
}

// Symbol ...
type Symbol struct {
	Coin string `json:"coin"`
	Currency string `json:"currency"`
}

// Ticker ...
type Ticker struct {
	Symbol   Symbol `json:"symbol"`
	BidPrice float64 `json:"bidPrice"`
	BidQty   float64 `json:"bidQty"`
	AskPrice float64 `json:"askPrice"`
	AskQty   float64 `json:"askQty"`
	BaseVolume float64 `json:"baseVolume"`
	QuoteVolume float64 `json:"quoteVolume"`
	Exchange string `json:"exchange"`
	Timestamp int `json:"timestamp"`
}
