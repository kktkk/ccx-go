package pkg

import "time"

// ErrorCheck ...
func ErrorCheck (e error) {
	if e != nil {
		panic(e)
	}
}

// Status429 ...
func Status429(
	status int, 
	f func() interface{}, 
	) {
	if status == 429 {
		time.Sleep(2 * time.Second)
		f()
	}
}