package cmd

import (
	"ccx/api/handlers"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
  Use:   "ccx",
  Short: "ccx is an application/wraper to handle crytocurrencies exchanges",
  Long: `ccx is an application/wraper to handle crytocurrencies exchanges 
	 with a unified interfaces.`,
  Run: func(cmd *cobra.Command, args []string) {
    // Do Stuff Here
  },
}

func Execute() {
  if err := rootCmd.Execute(); err != nil {
    fmt.Fprintln(os.Stderr, err)
    os.Exit(1)
  }
}

func init() {

	rootCmd.PersistentFlags().StringP("author", "a", "kktkk", "author name for copyright attribution")

	rootCmd.AddCommand(getExchangesCmd)

	getExchangesCmd.PersistentFlags().StringP("exchange_name", "e", "", "the name of the exchange")
	getExchangesCmd.MarkFlagRequired("exchange_name")
}

var getExchangesCmd = &cobra.Command{
  Use:   "exchanges",
  Short: "Get supported exchanges",
  Run: func(cmd *cobra.Command, args []string) {
		exchanges := handlers.GetExchanges()
    fmt.Printf("%v\n", exchanges)
  },
}