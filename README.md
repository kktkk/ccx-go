# CCX
This application aims to unify cryptocurrencies exchanges APIs. <br>
It was developed and is used in the project <a href="https://arbator.xyz/">Arbator</a>. <br>
Arbator is a crytocurrencies arbitrage platform.

## How to
### build
Depending on your OS and Architecture: <br>
`env GOOS=target-OS GOARCH=target-architecture go build -o <path/to/build>`

### install
After building run: <br>
`go install`

### run
`ccx -h, --help` <br>
To get available commands. <br>