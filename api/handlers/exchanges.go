package handlers

import (
	"ccx/pkg"
	"encoding/json"
	"net/http"
)

// GetExchanges ...
func GetExchanges () []string {

	var exchanges []string = []string{
		"binance",
		"bitfinex",
		"bittrex",
		"btcpop",
		"crypto.com",
		"graviex",
		"hitbtc",
		"poloniex",
		"stakecube",
	}

	return exchanges
}


// GetExchangesResponse ...
func GetExchangesResponse (w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", CORS)
	
	w.WriteHeader(http.StatusOK)

	exchanges := GetExchanges()

	jsonEx, err := json.Marshal(exchanges)

	pkg.ErrorCheck(err)

	rsp := pkg.Response{
		Code: 200,
		Msg: "ok!",
		Results: jsonEx,
	}

	jsonRsp, err := json.Marshal(rsp)
	pkg.ErrorCheck(err)
	w.Write(jsonRsp)
}
