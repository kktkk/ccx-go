package handlers

import (
	"ccx/internal/binance"
	"ccx/internal/bitfinex"
	"ccx/internal/bittrex"
	"ccx/internal/btcpop"
	"ccx/internal/graviex"
	"ccx/internal/hitbtc"
	"ccx/internal/poloniex"
	"ccx/internal/stakecube"
	"ccx/pkg"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

var SymbolsFuncs = map[string]func() []string {
	"binance": binance.GetSymbols,
	"bitfinex": bitfinex.GetSymbols,
	"bittrex": bittrex.GetSymbols,
	"btcpop": btcpop.GetSymbols,
	"graviex": graviex.GetSymbols,
	"hitbtc": hitbtc.GetSymbols,
	"poloniex": poloniex.GetSymbols,
	"stakecube": stakecube.GetSymbols,
}

// GetSymbols ...
func GetSymbols (w http.ResponseWriter, r *http.Request) {
	
	w.Header().Set("Access-Control-Allow-Origin", CORS)
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)

	exchange := vars["exchange"]

	symbols := SymbolsFuncs[exchange]()

	jsonSymbols, err := json.Marshal(symbols)

	pkg.ErrorCheck(err)

	rsp := pkg.Response{
		Code: 200,
		Msg: "ok",
		Results: jsonSymbols,
	}

	if len(symbols) == 0 {
		rsp.Code = 404
		rsp.Msg = "Error fetching tickers"
		rsp.Results = nil
	}

	jsonRsp, err := json.Marshal(rsp)
	pkg.ErrorCheck(err)
	w.Write(jsonRsp)
}