package handlers

import (
	"ccx/internal/binance"
	"ccx/internal/bittrex"
	"ccx/internal/btcpop"
	"ccx/internal/graviex"
	"ccx/internal/hitbtc"
	"ccx/internal/poloniex"
	"ccx/internal/stakecube"
)

// UrlFuncs ...
var UrlFuncs = map[string]func(string, string) string {
	"binance": binance.GenerateTradeUrl,
	"bittrex": bittrex.GenerateTradeUrl,
	"btcpop": btcpop.GenerateTradeUrl,
	"graviex": graviex.GenerateTradeUrl,
	"hitbtc": hitbtc.GenerateTradeUrl,
	"poloniex": poloniex.GenerateTradeUrl,
	"stakecube": stakecube.GenerateTradeUrl,
}