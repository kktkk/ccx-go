package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"ccx/internal/binance"
	"ccx/internal/bitfinex"
	"ccx/internal/bittrex"
	"ccx/internal/btcpop"
	"ccx/internal/graviex"
	"ccx/internal/hitbtc"
	"ccx/internal/poloniex"
	"ccx/internal/stakecube"
	"ccx/pkg"
)

var TickersFuncs = map[string]func() []pkg.Ticker {
	"binance": binance.GetTickers,
	"bitfinex": bitfinex.GetTickers,
	"bittrex": bittrex.GetTickers,
	"btcpop": btcpop.GetTickers,
	"graviex": graviex.GetTickers,
	"hitbtc": hitbtc.GetTickers,
	"poloniex": poloniex.GetTickers,
	"stakecube": stakecube.GetTickers,
}

// GetTickers ...
func GetTickers(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", CORS)
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)

	exchange := vars["exchange"]
	
	w.Write([]byte(exchange))

	tickers := TickersFuncs[exchange]()

	jsonTickers, err := json.Marshal(tickers)

	pkg.ErrorCheck(err)
	
	rsp := pkg.Response{
		Code: 200,
		Msg: "ok",
		Results: jsonTickers,
	}

	if len(tickers) == 0 {
		rsp.Code = 404
		rsp.Msg = "Error fetching tickers"
		rsp.Results = nil
	}

	jsonRsp, err := json.Marshal(rsp)
	pkg.ErrorCheck(err)
	w.Write(jsonRsp)	
}
