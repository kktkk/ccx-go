package handlers

import (
	"ccx/internal/binance"
	"ccx/internal/bitfinex"
	"ccx/internal/bittrex"
	"ccx/internal/graviex"
	"ccx/internal/hitbtc"
	"ccx/internal/poloniex"
	"net/http"
)

var CreateOrderFuncs = map[string]func(key, secret, side, symbol string, price, amount float64) string {
	"binance": binance.CreateOrder,
	"bitfinex": bitfinex.CreateOrder,
	"bittrex": bittrex.CreateOrder,
	"graviex": graviex.CreateOrder,
	"hitbtc": hitbtc.CreateOrder,
	"poloniex": poloniex.CreateOrder,
}

var CheckOrderFuncs = map[string]func(key, secret, symbol, orderId string) interface{} {
	"binance": binance.CheckOrder,
	"bitfinex": bitfinex.CheckOrder,
	"bittrex": bittrex.CheckOrder,
	"graviex": graviex.CheckOrder,
	"hitbtc": hitbtc.CheckOrder,
	"poloniex": poloniex.CheckOrder,
}

var CancelOrderFuncs = map[string]func(key, secret, symbol, orderId string) bool {
	"binance": binance.CancelOrder,
	"bitfinex": bitfinex.CancelOrder,
	"bittrex": bittrex.CancelOrder,
	"graviex": graviex.CancelOrder,
	"hitbtc": hitbtc.CancelOrder,
	"poloniex": poloniex.CancelOrder,
}


// CreateOrder ...
func CreateOrder (w http.ResponseWriter, r *http.Request) {}

// CheckOrder ...
func CheckOrder (w http.ResponseWriter, r *http.Request) {}

// CancelOrder ...
func CancelOrder (w http.ResponseWriter, r *http.Request) {}