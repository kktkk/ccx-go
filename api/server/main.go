package server

import (
	"ccx/api/handlers"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

var host = "0.0.0.0:80"

func RunServer() {
	log.Println("Starting server mode")
	log.Println("Press 'ctrl + c' to stop")
	log.Println("serving app: http://" + host)

	r := mux.NewRouter()
	
	r.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", handlers.CORS)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("ok"))
	})
	
	r.HandleFunc("/exchanges", handlers.GetExchangesResponse).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/symbols/{exchange}", handlers.GetSymbols).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/ticker/{exchange}", handlers.GetTickers).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/create_order/{exchange}", handlers.CreateOrder).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/check_order/{exchange}", handlers.CheckOrder).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/cancel_order/{exchange}", handlers.CancelOrder).Methods(http.MethodPost, http.MethodOptions)

	srv := &http.Server{
		Handler: r,
		Addr:    host,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: time.Second * time.Duration(30),
		ReadTimeout:  time.Second * time.Duration(30),
		IdleTimeout:  time.Second * time.Duration(30),
	}

	log.Println(srv.ListenAndServe())
}